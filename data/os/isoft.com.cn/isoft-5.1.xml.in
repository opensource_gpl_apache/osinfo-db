<libosinfo version="0.0.1">
<!-- Licensed under the GNU General Public License version 2 or later.
     See http://www.gnu.org/licenses/ for a copy of the license text -->
  <os id="http://isoft.com.cn/isoft/5.1">
    <short-id>isoft5.1</short-id>
    <name>isoft Linux 5.1</name>
    <version>5.1</version>
    <vendor>isoft, Inc</vendor>
    <family>linux</family>
    <distro>isoft</distro>
    <codename>Santiago</codename>
    <upgrades id="http://isoft.com.cn/isoft/4.2"/>
    <derives-from id="http://isoft.com.cn/isoft/4.2"/>

    <release-date>2013-11-21</release-date>
    <eol-date>2023-11-30</eol-date>

    <media arch="i686">
      <iso>
        <system-id>LINUX</system-id>
        <volume-id>.*isoft5.1 i386.*</volume-id>
      </iso>
      <kernel>isolinux/vmlinuz</kernel>
      <initrd>isolinux/initrd.img</initrd>
    </media>
    <media arch="x86_64">
      <iso>
        <system-id>LINUX</system-id>
        <volume-id>.*isoft5.1 x86_64.*</volume-id>
      </iso>
      <kernel>isolinux/vmlinuz</kernel>
      <initrd>isolinux/initrd.img</initrd>
    </media>
    <media arch="mips64el">
      <iso>
        <system-id>LINUX</system-id>
        <volume-id>.*isoft5.1 mips64el.*</volume-id>
      </iso>
      <kernel>isolinux/vmlinuz</kernel>
      <initrd>isolinux/initrd.img</initrd>
    </media>
    <media arch="aarch64">
      <iso>
        <system-id>LINUX</system-id>
        <volume-id>.*isoft5.1 aarch64.*</volume-id>
      </iso>
      <kernel>isolinux/vmlinuz</kernel>
      <initrd>isolinux/initrd.img</initrd>
    </media>        

    <tree arch="ppc64">
      <treeinfo>
        <family>isoft Linux</family>
        <version>^5.1$</version>
        <arch>ppc64</arch>
      </treeinfo>
    </tree>

    <tree arch="s390x">
      <treeinfo>
        <family>isoft Linux</family>
        <version>^5.1$</version>
        <arch>s390x</arch>
      </treeinfo>
    </tree>
    
    <tree arch="mips64el">
      <treeinfo>
        <family>isoft Linux</family>
        <version>^5.1$</version>
        <arch>mips64el</arch>
      </treeinfo>
    </tree>
    
    <tree arch="aarch64">
      <treeinfo>
        <family>isoft Linux</family>
        <version>^5.1$</version>
        <arch>aarch64</arch>
      </treeinfo>
    </tree>

    <tree arch="i686">
      <treeinfo>
        <family>isoft Linux</family>
        <version>^5.1$</version>
        <arch>i386</arch>
      </treeinfo>
    </tree>

    <tree arch="x86_64">
      <treeinfo>
        <family>isoft Linux</family>
        <version>^5.1$</version>
        <arch>x86_64</arch>
      </treeinfo>
    </tree>

    <resources arch="all">
      <minimum>
        <n-cpus>1</n-cpus>
        <ram>536870912</ram>
        <storage>1073741824</storage>
      </minimum>

      <recommended>
        <cpu>400000000</cpu>
        <ram>1073741824</ram>
        <storage>5368709120</storage>
      </recommended>
    </resources>

    <resources arch="i686">
      <minimum>
        <ram>536870912</ram>
        <storage>1073741824</storage>
      </minimum>

      <recommended>
        <cpu>400000000</cpu>
        <ram>1073741824</ram>
        <storage>9663676416</storage>
      </recommended>

      <maximum>
        <n-cpus>32</n-cpus>
        <ram>17179869184</ram>
      </maximum>
    </resources>

    <resources arch="x86_64">
      <minimum>
        <ram>1073741824</ram>
        <storage>1073741824</storage>
      </minimum>

      <recommended>
        <cpu>400000000</cpu>
        <ram>1073741824</ram>
        <storage>9663676416</storage>
      </recommended>

      <maximum>
        <n-cpus>288</n-cpus>
        <ram>3298534883328</ram>
      </maximum>
    </resources>

    <installer>
      <script id='http://redhat.com/rhel/kickstart/jeos'/>
      <script id='http://redhat.com/rhel/kickstart/desktop'/>
    </installer>
  </os>
</libosinfo>
